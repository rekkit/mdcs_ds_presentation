import pandas as pd
import pickle
import numpy as np
import scipy
from sklearn import model_selection, metrics, ensemble, preprocessing
from xgboost import XGBClassifier
from rekkml import parameter_search, encoding
import plotly
from plotly import graph_objs as go

# Load data
train = pd.read_csv(".\\data\\train.csv")
test = pd.read_csv(".\\data\\test.csv")

# Check data types for one-hot encoding
encoding.check_categorical(train)
encoding.check_categorical(test)

# now let's do one-hot encoding
train = pd.get_dummies(train)
test = pd.get_dummies(test)

"""
XGB
"""
# using random parameter search (not grid search)
xgb_parameter_distribution = dict(colsample_bytree=scipy.stats.uniform(0.2, 0.8),
                                  subsample=scipy.stats.uniform(0.1, 0.9),
                                  gamma=scipy.stats.uniform(),
                                  learning_rate=scipy.stats.uniform(0.0001, 0.1),
                                  max_depth=scipy.stats.randint(1, 15 + 1),
                                  min_child_weight=scipy.stats.randint(1, 10 + 1),
                                  n_estimators=scipy.stats.randint(50, 150 + 1))

xgb_param_search = model_selection.RandomizedSearchCV(estimator=XGBClassifier(),
                                                      param_distributions=xgb_parameter_distribution,
                                                      n_iter=100,
                                                      cv=10,
                                                      verbose=20,
                                                      n_jobs=1)

xgb_param_search.fit(train.drop("converted", axis=1), train.converted)

# To binary
psr_xgb = parameter_search.param_search_res(xgb_param_search.cv_results_)
pickle.dump(psr_xgb, open(".\\psr\\psr_xgb", "wb"))

"""
After fitting
"""

# Fresh load
psr_xgb = pickle.load(open(".\\psr\\psr_xgb", "rb"))

# Define the learner
xgbm = XGBClassifier(**psr_xgb.get_model_params(12))

# Fit
xgbm.fit(train.drop("converted", axis=1), train.converted.values)

# Predicting
pred_xgb = xgbm.predict(test.drop("converted", axis=1))

# XGB scores
metrics.accuracy_score(test.converted.values, pred_xgb) * 100
metrics.precision_score(test.converted.values, pred_xgb) * 100
metrics.recall_score(test.converted.values, pred_xgb) * 100

# Let's plot feature importance
temp = pd.DataFrame.from_dict({"feature_importance": xgbm.feature_importances_,
                               "feature": train.drop("converted", axis=1).columns}).sort_values(by="feature_importance",
                                                                                                ascending=False)
g = go.Bar(x=temp.feature,
           y=temp.feature_importance)

plotly.offline.plot([g])
